package aws.mitocode.monolitico.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import aws.mitocode.monolitico.model.Negocio;

@Repository
public interface INegocioDao extends JpaRepository<Negocio, Integer> {

}
