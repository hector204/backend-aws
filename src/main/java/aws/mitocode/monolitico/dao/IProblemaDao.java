package aws.mitocode.monolitico.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import aws.mitocode.monolitico.model.Problema;

@Repository
public interface IProblemaDao extends JpaRepository<Problema, Integer> {

}
