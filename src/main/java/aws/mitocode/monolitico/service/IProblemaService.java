package aws.mitocode.monolitico.service;

import java.util.List;

import aws.mitocode.monolitico.model.Problema;

public interface IProblemaService {
	
	public List<Problema> obtenerTodos();
}
