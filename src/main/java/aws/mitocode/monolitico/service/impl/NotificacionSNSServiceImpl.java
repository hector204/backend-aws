package aws.mitocode.monolitico.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.fasterxml.jackson.databind.ObjectMapper;

import aws.mitocode.monolitico.model.FeedBack;
import aws.mitocode.monolitico.service.INotificacionSNS;

@Service
public class NotificacionSNSServiceImpl implements INotificacionSNS {
	
	private Logger logger = Logger.getLogger(NotificacionSNSServiceImpl.class);

	private static final String ARN_TOPICO_PROCESA_FEEDBACK = "arn:aws:sns:us-east-1:287557457729:procesarFeedBack";

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private AmazonSNSClient servicioSNS;

	public void enviarNotificacionSubscriptores(FeedBack feedback) {
		try {
			PublishRequest publishRequest = new PublishRequest(ARN_TOPICO_PROCESA_FEEDBACK,
					mapper.writeValueAsString(feedback));
			
			PublishResult publishResult = servicioSNS.publish(publishRequest);
			
			logger.info("MessageId - " + publishResult.getMessageId());
		} catch (Exception e) {
			logger.error("Error al enviar mensaje a SNS");
		}
	}
}
