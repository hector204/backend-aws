package aws.mitocode.monolitico.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aws.mitocode.monolitico.dao.IProblemaDao;
import aws.mitocode.monolitico.model.Problema;
import aws.mitocode.monolitico.service.IProblemaService;

@Service
public class ProblemaServiceImpl implements IProblemaService {

	@Autowired
	private IProblemaDao problemaDao;
	
	@Override
	public List<Problema> obtenerTodos() {
		return this.problemaDao.findAll();
	}

}
