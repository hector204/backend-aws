package aws.mitocode.monolitico.service;

import aws.mitocode.monolitico.model.FeedBack;

public interface IEmailService {
	
	public boolean sendEmail(FeedBack feedback);
}
