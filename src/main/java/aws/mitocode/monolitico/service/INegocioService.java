package aws.mitocode.monolitico.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import aws.mitocode.monolitico.model.Negocio;

public interface INegocioService {

	List<Negocio> obtenerTodos();
	Page<Negocio> obtenerDatosPaginados(Pageable pageable);
	void guardarDatos(Negocio negocio);
	void eliminarDatos(int id);
}
