package aws.mitocode.monolitico.service;

import aws.mitocode.monolitico.model.FeedBack;

public interface INotificacionSNS {

	public void enviarNotificacionSubscriptores(FeedBack feedback);
}
