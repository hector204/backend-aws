package aws.mitocode.monolitico.service;

import java.io.IOException;

import aws.mitocode.monolitico.model.FeedBack;

public interface IColaSQSService {

	public void readJMS(String feedback);
	public void sendDataJMS(FeedBack feedBack) throws IOException;
}
