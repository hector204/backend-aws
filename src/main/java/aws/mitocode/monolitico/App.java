package aws.mitocode.monolitico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.sns.AmazonSNSClient;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories
public class App /*extends SpringBootServletInitializer */{

//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(App.class);
//	}

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
	
	@Bean
	public AmazonSimpleEmailService crearSES() {
		AmazonSimpleEmailService clienteSES = new AmazonSimpleEmailServiceClient( new DefaultAWSCredentialsProviderChain() );
		clienteSES.setRegion(Region.getRegion(Regions.US_EAST_1));
		return clienteSES;
	}
	
	@Bean
	public AmazonSNSClient crearSNS() {
		AmazonSNSClient clienteSNS = new AmazonSNSClient(new DefaultAWSCredentialsProviderChain());		                           
		clienteSNS.setRegion(Region.getRegion(Regions.US_EAST_1));
		return clienteSNS;
	}
}
